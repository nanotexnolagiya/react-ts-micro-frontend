import React, { lazy, Suspense } from 'react';
import { render } from "react-dom";

import Main from './main';
// import CustomerMain from 'CustomersApp/App';
// import SupplierMain from 'SupplierApp/App';

// const Main = lazy(() => import("./main"));

const CustomerMain = lazy(() => import("CustomersApp/App"));

const SupplierMain = lazy(() => import("SupplierApp/App"));

const hosts = window.location.host.split(".");

const otherMains = {
  customer: <CustomerMain />,
  supplier: <SupplierMain />,
  admin: <Main />,
};

const container = document.getElementById("app");

if (hosts.length < 2) {
  render(<Main />, container);
} else {
  render(
    <Suspense fallback={<div>Instance Loading...</div>}>
      {otherMains[hosts[0]]}
    </Suspense>,
    container
  );
}
