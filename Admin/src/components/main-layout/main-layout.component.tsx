import React from "react";
import styles from "./main-layout.scss";
import { useMainLayoutProps } from "./main-layout.props";
import { Layout, Menu, Button, Dropdown, Row, Col } from "antd";
import { hoc } from "@packages/utils";

const { Header, Content, Footer, Sider } = Layout;

/**
 * <MainLayout />
 */
const MainLayout = hoc(
  useMainLayoutProps,
  ({ children, onBreakpoint, onCollapse, sidebarMenuList, onLogout }) => (
    <Layout className={styles.layout}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={onBreakpoint}
        onCollapse={onCollapse}
      >
        <div className={styles.logo}>Admin</div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["4"]}>
          {sidebarMenuList.map((menu) => (
            <Menu.Item key={menu.title}>{menu.title}</Menu.Item>
          ))}
        </Menu>
      </Sider>
      <Layout>
        <Header className={styles.siteLayoutSubHeaderBackground}>
          <Row justify="end">
            <Col>
              <Dropdown
                overlay={
                  <Menu>
                    <Menu.Item onClick={onLogout}>Logout</Menu.Item>
                  </Menu>
                }
                placement="bottomRight"
              >
                <Button>Settings</Button>
              </Dropdown>
            </Col>
          </Row>
        </Header>
        <Content style={{ margin: "24px 16px 0" }}>
          <div
            className={styles.siteLayoutBackground}
            style={{ padding: 24, minHeight: 360 }}
          >
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>Footer description</Footer>
      </Layout>
    </Layout>
  )
);

export { MainLayout };
