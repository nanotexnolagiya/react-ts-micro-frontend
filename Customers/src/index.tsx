import React, { lazy, Suspense } from 'react';
import { render } from 'react-dom';
import Main from './main';

// const hosts = window.location.host.split('.')

// const SupplierMain = lazy(() => import("SupplierApp/App"));

// const AdminMain = lazy(() => import("AdminApp/App"));

// const otherMains = {
//   customer: <Main />,
//   supplier: <SupplierMain />,
//   admin: <AdminMain />
// }

const container = document.getElementById("app")

render(<Main />, container);

// if (hosts.length < 2) {
//   render(<AdminMain />, container);
// } else {
//   render(
//     otherMains[hosts[0]],
//     container
//   );
// }
