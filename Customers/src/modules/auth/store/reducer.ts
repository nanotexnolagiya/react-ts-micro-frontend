import {} from "@auth/models";
import { reducer } from "redux-chill";
import { signIn } from "./actions";
import { AuthState } from "./state";

const auth = reducer(new AuthState()).on(signIn.failure, (state, message) => {
  state.error = message;
});

export { auth };
