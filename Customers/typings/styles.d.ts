declare module '*.scss' {
  const content: { [className: string]: string };
  export = content;
}

declare module '*.css' {
  const content: { [className: string]: string };
  export = content;
}

declare module '*.png' {
  export = any;
}

declare module '*.svg' {
  export = any;
}

declare module "AdminApp/App" {
  const CounterAppOne: React.ComponentType;

	export default CounterAppOne; 
}

declare module "SupplierApp/App" {
  const CounterAppOne: React.ComponentType;

	export default CounterAppOne; 
}
