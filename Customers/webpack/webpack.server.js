const webpackConfig = require('./webpack.dev');

const { merge } = require('webpack-merge');

/**
 * Dev server config
 */
module.exports = merge(webpackConfig, {
  devServer: {
    host: 'localhost',

    port: 3001,
    
    hot: true,

    quiet: true,

    inline: true,

    publicPath: '/',

    contentBase: '../dist',

    disableHostCheck: true,

    historyApiFallback: true
  }
});
