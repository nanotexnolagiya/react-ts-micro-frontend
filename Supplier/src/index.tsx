import React, { lazy, Suspense } from 'react';
import { render } from 'react-dom';
import Main from './main';

// const Main = lazy(() => import("./main"));

// const CustomerMain = lazy(() => import("CustomersApp/App"));

// const AdminMain = lazy(() => import("AdminApp/App"));

// const hosts = window.location.host.split('.')

// const otherMains = {
//   customer: <CustomerMain />,
//   supplier: <Main />,
//   admin: <AdminMain />
// }

const container = document.getElementById("app")

render(<Main />, container);

// if (hosts.length < 2) {
//   render(<AdminMain />, container);
// } else {
//   render(
//     otherMains[hosts[0]],
//     container
//   );
// }
