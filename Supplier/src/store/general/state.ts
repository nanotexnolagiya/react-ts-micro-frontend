import { Account } from "@models";


class GeneralState {
  public ready: boolean = false;
  
  public account: Account
}

export { GeneralState };
